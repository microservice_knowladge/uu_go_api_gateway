package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"microservice_knowladge/uu_go_api_gateway/api/http"
	"microservice_knowladge/uu_go_api_gateway/api/models"
	"microservice_knowladge/uu_go_api_gateway/genproto/user_service"
	"microservice_knowladge/uu_go_api_gateway/pkg/helper"
	"microservice_knowladge/uu_go_api_gateway/pkg/util"
)

// CreateHobby godoc
// @ID create_hobby
// @Router /hobby [POST]
// @Summary Create hobby
// @Description  Create hobby
// @Tags hobby
// @Accept json
// @Produce json
// @Param profile body hobby_service.CreateHobbies true "CreatehobbyRequestBody"
// @Success 200 {object} http.Response{data=hobby_service.hobby} "GethobbyBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateHobby(c *gin.Context) {

	var hobby user_service.CreateHobbies

	err := c.ShouldBindJSON(&hobby)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.HobbyService().Create(
		c.Request.Context(),
		&hobby,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GethobbyByID godoc
// @ID get_hobby_by_id
// @Router /hobby/{id} [GET]
// @Summary Get hobby  By ID
// @Description Get hobby  By ID
// @Tags hobby
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=hobby_service.hobby} "hobbyBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetHobbyByID(c *gin.Context) {

	hobbyID := c.Param("id")

	if !util.IsValidUUID(hobbyID) {
		h.handleResponse(c, http.InvalidArgument, "hobby id is an invalid uuid")
		return
	}

	resp, err := h.services.HobbyService().GetByID(
		context.Background(),
		&user_service.HobbiesPrimaryKey{
			Id: hobbyID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GethobbyList godoc
// @ID get_hobby_list
// @Router /hobby [GET]
// @Summary Get hobby s List
// @Description  Get hobby s List
// @Tags hobby
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=hobby_service.GetListhobbyResponse} "GetAllhobbyResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetHobbyList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.HobbyService().GetList(
		context.Background(),
		&user_service.GetListHobbiesRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// Updatehobby godoc
// @ID update_hobby
// @Router /hobby/{id} [PUT]
// @Summary Update hobby
// @Description Update hobby
// @Tags hobby
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body hobby_service.Updatehobby true "UpdatehobbyRequestBody"
// @Success 200 {object} http.Response{data=hobby_service.hobby} "hobby data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateHobby(c *gin.Context) {

	var hobby user_service.UpdateHobbies

	hobby.Id = c.Param("id")

	if !util.IsValidUUID(hobby.Id) {
		h.handleResponse(c, http.InvalidArgument, "hobby id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&hobby)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.HobbyService().Update(
		c.Request.Context(),
		&hobby,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// Patchhobby godoc
// @ID patch_hobby
// @Router /hobby/{id} [PATCH]
// @Summary Patch hobby
// @Description Patch hobby
// @Tags hobby
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=hobby_service.hobby} "hobby data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchHobby(c *gin.Context) {

	var updatePatchhobby models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchhobby)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchhobby.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchhobby.ID) {
		h.handleResponse(c, http.InvalidArgument, "hobby id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchhobby.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.HobbyService().UpdatePatch(
		c.Request.Context(),
		&user_service.UpdatePatchHobbies{
			Id:     updatePatchhobby.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// Deletehobby godoc
// @ID delete_hobby
// @Router /hobby/{id} [DELETE]
// @Summary Delete hobby
// @Description Delete hobby
// @Tags hobby
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "hobby data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteHobby(c *gin.Context) {

	hobbyId := c.Param("id")

	if !util.IsValidUUID(hobbyId) {
		h.handleResponse(c, http.InvalidArgument, "hobby id is an invalid uuid")
		return
	}

	resp, err := h.services.HobbyService().Delete(
		c.Request.Context(),
		&user_service.HobbiesPrimaryKey{Id: hobbyId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
