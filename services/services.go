package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"microservice_knowladge/uu_go_api_gateway/config"
	"microservice_knowladge/uu_go_api_gateway/genproto/user_service"
)

type ServiceManagerI interface {
	UserService() user_service.UserServiceClient
	HobbyService() user_service.HobbyServiceClient
}

type grpcClients struct {
	userService  user_service.UserServiceClient
	hobbyService user_service.HobbyServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:  user_service.NewUserServiceClient(connUserService),
		hobbyService: user_service.NewHobbyServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) HobbyService() user_service.HobbyServiceClient {
	return g.hobbyService
}
